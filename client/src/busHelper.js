const jsSHA = require('jssha');
const axios = require('axios');

// header info
const AppID = '3c148316d9f24d4fa15772be932613d9';
const AppKey = 'hQzI2Xp97IU8B7jNzZAGdmU3ZXQ';
const GMTString = new Date().toGMTString();
const ShaObj = new jsSHA('SHA-1', 'TEXT');
ShaObj.setHMACKey(AppKey, 'TEXT');
ShaObj.update('x-date: ' + GMTString);
const HMAC = ShaObj.getHMAC('B64');
const Authorization = 'hmac username="' + AppID + '", algorithm="hmac-sha1", headers="x-date", signature="' + HMAC + '"';
const authorizationHeader = {
  'Authorization': Authorization,
  'X-Date': GMTString
  /*'Accept-Encoding': 'gzip'*/
};

class BusServer {
  // Get Result data
  static getRouteList (route) {
    const api = 'https://ptx.transportdata.tw/MOTC/v2/Bus/DisplayStopOfRoute/City/Taipei/' + route;
    const newAxios = axios.create({
      headers: authorizationHeader
    });

    return new Promise (async(resolve, reject) => {
      try {
        const res = await newAxios.get(api);
        const data = res.data;

        // remove duplicate item
        let list = [];
        for(let i = 0; i < data.length; i++) {
          let isDuplicate = false;
          for(let j = 0; j < list.length; j++) {
            if(data[i].RouteName.Zh_tw === list[j]) {
              isDuplicate = true;
              break;
            }
          }
          if(!isDuplicate) {
            list.push(data[i].RouteName.Zh_tw);
          }
        }

        resolve(list);
      } catch(err) {
        reject(err);
      }
    });
  }

  // get route info
  static getRouteInfo (route) {
    // TODO: change into static data
    const api = 'https://ptx.transportdata.tw/MOTC/v2/Bus/Route/City/Taipei/' + route +'?$top=30&$format=JSON&$filter=RouteName%2FZh_tw%20eq%20%27' + route + '%27';
    const newAxios = axios.create({
      headers: authorizationHeader,
    });

    return new Promise (async(resolve, reject) => {
      try {
        const res = await newAxios.get(api);
        const data = res.data[0];
        resolve({
          origin: data.DepartureStopNameZh,
          destination: data.DestinationStopNameZh
        });
      } catch(err) {
        reject(err);
      }
    });
  }

  // get stops estimated time
  static getEstimatedTimeList (route) {
    const api = "https://ptx.transportdata.tw/MOTC/v2/Bus/EstimatedTimeOfArrival/City/Taipei/" + route + "?$top=200&$format=JSON&$filter=RouteName/Zh_tw%20eq%20%27" + route + "%27";
    const newAxios = axios.create({
      headers: authorizationHeader
    });

    return new Promise (async(resolve, reject) => {
      try {
        const res = await newAxios.get(api);
        const list = res.data;

        // sort estimatedList
        let estimatedList = [[], []];
        for(let i = 0; i < list.length; i++) {
          if(list[i].Direction === 0) {
            estimatedList[0].push(list[i]);
          }
          else {
            estimatedList[1].push(list[i]);
          }
        }
        resolve(estimatedList); //[[去程], [回程]]
      } catch(err) {
        reject(err);
      }
    });
  }

  static getRoute (route) {
    const api = 'https://ptx.transportdata.tw/MOTC/v2/Bus/DisplayStopOfRoute/City/Taipei/' + route + '?$top=10&$format=JSON&$filter=RouteName/Zh_tw%20eq%20%27' + route + '%27';
    const newAxios = axios.create({
      headers: authorizationHeader
    });

    return new Promise (async(resolve, reject) => {
      try {
        const res = await newAxios.get(api);
        const list = res.data;
        resolve(list); // [{去程}, {回程}] or [{單程}]
      } catch(err) {
        reject(err);
      }
    });
  }
}

export default BusServer;