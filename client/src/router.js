import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Result from './views/Result.vue'
import Detail from './views/Detail.vue'
import Login from './views/Login.vue'
import Register from './views/Register.vue'
import Favorites from './views/Favorites.vue'
import store from './store/index'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/results',
      name: 'results',
      component: Result
    },
    {
      path: '/detail/:id',
      name: 'detail',
      component: Detail
    },
    {
      path: '/favorites',
      name: 'favorites',
      component: Favorites,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
        notLoggedIn: true
      }
    },
    {
      path: '/register',
      name: 'register',
      component: Register,
      meta: {
        notLoggedIn: true
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.state.user) {
      next()
      return
    }
    next('/login')
  } else if (to.matched.some(record => record.meta.notLoggedIn)) {
    if (!store.state.user) {
      next()
      return
    }
    next('/')
  } else {
    next()
  }
})

export default router