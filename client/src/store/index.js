import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
      sessionID: window.sessionStorage.getItem('sessionID') || null,
      status: '',
      user: null
    },
    mutations: {
      auth_success(state, id) {
        state.status = 'success'
        state.sessionID = id
      },
      auth_error(state) {
        state.status = 'err'
        state.sessionID = null
        state.user = null
      },
      fetch_user(state, user) {
        state.user = user
      },
      logout(state) {
        state.status = ''
        state.sessionID = null
        state.user = null
      },
    },
    actions: {
      login({ commit }, user) {
        const loginUrl = 'api/users/login'
        return new Promise (async(resolve, reject) => {
          try {
            const { data } = await axios.post(loginUrl, {
              username: user.username,
              password: user.password
            })
            if(data) {
              window.sessionStorage.setItem('sessionID', data.sessionID)
              commit('auth_success', data.sessionID)
              resolve(data)
            }
            else {
              commit('auth_error')
              reject('no user')
            }
          } catch(err) {
            commit('auth_error')
            reject(err)
          }
        })
      },
      logout({ commit }) {
        window.sessionStorage.removeItem('sessionID')
        commit('logout')
      },
      register({ commit }, user) {
        const registerUrl = 'api/users/register'
        return new Promise (async(resolve, reject) => {
          try {
            const res = await axios.post(registerUrl, user)
            if(res.data.status === 'success') {
              resolve(res)
            }
            else {
              commit('auth_error')
              reject(res.data)
            }
          } catch(err) {
            commit('auth_error')
            reject(err)
          }
        })
      },
      addFavorite({ commit, state }, route) {
        const addUrl = 'api/users/favorites/add'
        return new Promise (async(resolve, reject) => {
          try {
            const res = await axios.post(addUrl, {id: state.user._id, route: route})
            if(res.data.status === 'success') {
              commit('fetch_user', res.data.user)
              resolve(res)
            }
            else {
              reject(res)
            }
          } catch(err) {
            reject(err)
          }
        })
      },
      removeFavorite({ commit, state }, route) {
        const removeUrl = 'api/users/favorites/remove'
        return new Promise (async(resolve, reject) => {
          try {
            const res = await axios.post(removeUrl, {id: state.user._id, route: route})
            if(res.data.status === 'success') {
              commit('fetch_user', res.data.user)
              resolve(res)
            }
            else {
              reject(res.data)
            }
          } catch(err) {
            reject(err)
          }
        })
      },
      async getUserInfo ({ commit, state }) {
        const userInfoUrl = `api/users/userInfo/${state.sessionID}`
        try {
          const { data } = await axios.get(userInfoUrl)
          if (data.err) {
            commit('auth_error')
          } else {
            commit('fetch_user', data)
          }
        } catch(err) {
          // handle error
        }
      }
    }
})
