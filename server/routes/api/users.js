const express = require('express');
const mongoose = require('mongoose');
const passport = require('passport');
const bodyParser = require('body-parser');
const LocalStrategy = require('passport-local');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const ObjectId = require('mongodb').ObjectId
const User = require('../../models/user');

mongoose.connect('mongodb+srv://busAdmin:admin8246@cluster0-fzitc.mongodb.net/test?retryWrites=true&w=majority', {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

const app = express();

var store = new MongoDBStore({
  uri: 'mongodb+srv://busAdmin:admin8246@cluster0-fzitc.mongodb.net/test?retryWrites=true&w=majority',
  collection: 'sessions'
});

// required for passport session
app.use(session({
  secret: process.env.SESSION_SECRET || 'keyboard cat',
  saveUninitialized: false,
  resave: false,
  // using store session on MongoDB using express-session + connect
  store: store,
  cookie: {
    secure: false,
    maxAge: 3600000
  }
}));
app.use(bodyParser.urlencoded({extended: true}));
app.use(passport.initialize());
app.use(passport.session());

passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

// Functions
function findFromCollection (name, query, cb) {
  mongoose.connection.db.collection(name, function (err, collection) {
    collection.find(query).toArray(cb);
 });
}

// Register
app.post('/register', (req, res) => {
  User.register(new User({username: req.body.username}), req.body.password, (err, user) => {
    if(err) {
      res.json(err);
    }
    else {
      passport.authenticate('local')(req, res, () => {
        res.json({ 'status': 'success' });
      });
    }
  })
});

// Login
app.post('/login', function(req, res, next) {
  passport.authenticate('local', function(err, user, info) {
    if (err) { return res.json(err); }
    if (!user) { return res.json(err); }
    req.logIn(user, function(err) {
      if (err) { return res.json(err); }
      return res.json({
        sessionID: req.sessionID
      });
    });
  })(req, res, next);
});

// Logout
app.get('/logout', (req, res) => {
  req.logout();
});

// Add Favorite
app.post('/favorites/add', function(req, res, next) {
  User.findById({_id: new ObjectId(req.body.id)}, (err, user) => {
    const hasRoute = function(route) {
      for(let i = 0; i < user.favorites.length; i++) {
        if (route === user.favorites[i]) {
          return true
        }
      }
      return false
    }

    if(err) {
      res.json({ 'err': err })
    }
    else {
      if(!hasRoute(req.body.route)) {
        user.favorites.push(req.body.route)
        user.save()
        res.json({ 'user': user, 'msg': 'Add succeed', 'status': 'success' })
      }
      else {
        res.json({ 'msg': 'Already added', 'status': 'failed' })
      }
    }
  })
});


// Remove Favorite
app.post('/favorites/remove', function(req, res, next) {
  User.findById({_id: new ObjectId(req.body.id)}, (err, user) => {
    if(err) {
      res.json({ 'err': err })
    }
    else {
      for(let i = 0; i < user.favorites.length; i++) {
        if(user.favorites[i] === req.body.route) {
          user.favorites.splice(i, 1)
        }
      }
      user.save()
      res.json({ 'user': user, 'msg': 'Remove succeed', 'status': 'success' })
    }
  })
});

// Get User Info
app.get('/userInfo/:id', function(req, res, next) {
  let username = ''
  findFromCollection('sessions', {_id : req.params.id}, function (err, docs) {
    if (docs.length) {
      username = docs[0].session.passport.user
    }

    if (username) {
      User.find({username: username}, (err, users) => {
        if(err) {
          res.json({ 'err': err })
        } else {
          if (users.length) {
            res.json(users[0])
          }
        }
      })
    } else {
      res.json({ 'err': 'session expired' })
    }
  });
});

module.exports = app;